/*    fancontrol
 *
 * fancontrol is free software.
 * Copyright (C) 2017 Matteo Bonora (bonora.matteo@gmail.com) - All Rights Reserved
 *
 * fancontrol is available under the GNU LGPLv3 License which is available at <http://www.gnu.org/licenses/lgpl.html>

  This is a temperature-based fan controller using PID logic and PWM signals to control PC fans.

  Check out my instructable on this project for more info
  https://www.instructables.com/id/Temperature-Control-With-Arduino-and-PWM-Fans/
  
  The PWM frequency is set to 23437 Hz that is within the 21k-25k Hz range so it should work with any PC fan.
  For more details on how PWM works on the 32u4, check this link:

  http://r6500.blogspot.it/2014/12/fast-pwm-on-arduino-leonardo.html

  Note that the 32u4 has 2 pins (6 and 13) hooked to Timer4, but since my board doesn't have pin 13 I only configure pin 6.
  A RPM reading system is also featured in this example (although it has proven to be not that accurate, at least on my setup).

  This code has been tested on a SparkFun Pro Micro 16MHz Clone with 4 Arctic F12 PWM PST Fans connected to the same connector.
*/

#include <PID_v1.h>       // https://github.com/br3ttb/Arduino-PID-Library
#include <DHT.h>          // https://github.com/markruys/arduino-DHT
//#include <LedControl.h>   // https://github.com/wayoda/LedControl
#include <EEPROM.h>

// Change this if you want your current settings to be overwritten.
#define CONFIG_VERSION "f01"
// Where to store config data in EEPROM
#define CONFIG_START 32

#define THERMISTOR
#ifdef THERMISTOR
#define DUTY_MAX 192
#define DUTY_NOM 128
#define DUTY_OD 255
#define TARGET_SET 59.0
#define TEMP_CRITICAL 65
#define TEMP_MIN 33
#define TEMP_MIN0 31
#else
#define DUTY_MAX 156
#define DUTY_NOM 128
#define DUTY_OD 255
#define TARGET_SET 38.5
#define TEMP_CRITICAL 41
#define TEMP_MIN 33
#define TEMP_MIN0 31
#endif

double duty_set = 0.0;

// Pin 6 shortcut
#define PWM6        OCR4D

// Terminal count
#define PWM6_MAX OCR4C

/*        Pinouts       */
#define SPD_IN 7        // RPM input pin

// 7-segment display
#define SEG_DIN 16
#define SEG_CLK 14
#define SEG_CS 15

#define RELAY 10         // Relay output pin

#define TEMP_IN 4       // Temperature sensor input pin

#define DBG true
// Debug macro to print messages to serial
#define DEBUG(x)  if(DBG && Serial) { Serial.print (x); }

// Tells the amount of time (in ms) to wait between updates
#define WAIT 5000

#define DUTY_MIN 64        // The minimum fans speed (0...DUTY_OD)
//#define DUTY_DEAD_ZONE 64  // The delta between the minimum output for the PID and DUTY_MIN (DUTY_MIN - DUTY_DEAD_ZONE).
#define DUTY_DEAD_ZONE 4  // The delta between the minimum output for the PID and DUTY_MIN (DUTY_MIN - DUTY_DEAD_ZONE).

/* Target set vars */
bool targetMode = false;
bool lastUp = false, lastDown = false;
bool up, down = false;

// debug monitor output count
#define DEBUG_FREQ 1
int debug10 = 1;
// count number of times we could not read temperature
int notemp_cnt = 0;

// What pin to connect the DVM to
#define DVMPIN A0
// What pin to connect the thermistor sensor to
#define THERMISTORPIN A2
#define NUMSAMPLES 5
int samples[NUMSAMPLES];
// resistance at 25 degrees C
#define THERMISTORNOMINAL 10000      
#define SERIESRESISTOR 10000    
// temp. for nominal resistance (almost always 25 C)
#define TEMPERATURENOMINAL 25   
// how many samples to take and average, more takes longer
// but is more 'smooth'
#define NUMSAMPLES 5
// The beta coefficient of the thermistor (usually 3000-4000)
#define BCOEFFICIENT 3950
// the value of resistor in series of Thermistor

/* RPM calculation */
volatile unsigned long duration = 0; // accumulates pulse width
volatile unsigned int pulsecount = 0;
volatile unsigned long previousMicros = 0;
int ticks = 0, speed = 0;

unsigned long prev1, prev2, prev3 = 0; // Time placeholders

double duty;
// Display temp, .5 rounded and Compute temp, integer (declared as double because of PID library input);
double dtemp, ctemp, rtemp;

// Fan status
bool fanRunning = false;

// Settings
struct StoreStruct
{
  // This is for mere detection if they are your settings
  char version[4];
  // The variables of your settings
  double target;
} storage = { // Default values
  CONFIG_VERSION,
  40
};

// Initialize all the libraries.
#define KP 0.4
#define KI 0.4
#define KD 0.05
PID fanPID(&ctemp, &duty, &storage.target, KP, KI, KD, REVERSE);

//LedControl lc = LedControl(SEG_DIN, SEG_CLK, SEG_CS, 1);
DHT sensor;

/* Configure the PWM clock */
void pwm6configure()
{
  // TCCR4B configuration
  TCCR4B = 4; /* 4 sets 23437Hz */

  // TCCR4C configuration
  TCCR4C = 0;

  // TCCR4D configuration
  TCCR4D = 0;

  // PLL Configuration
  PLLFRQ = (PLLFRQ & 0xCF) | 0x30;

  // Terminal count for Timer 4 PWM
  OCR4C = 255;
}

// Set PWM to D6 (Timer4 D)
// Argument is PWM between 0 and 255
void pwmSet6(int value)
{
  OCR4D = value;  // Set PWM value
  DDRD |= 1 << 7; // Set Output Mode D7
  TCCR4C |= 0x09; // Activate channel D
}

/* Called when hall sensor pulses */
void pickRPM ()
{
  volatile unsigned long currentMicros = micros();

  if (currentMicros - previousMicros > 20000) // Prevent pulses less than 20k micros far.
  {
    duration += currentMicros - previousMicros;
    previousMicros = currentMicros;
    ticks++;
  }
}

/* Settings management on the EEPROM */
void loadConfig()
{
  // Check if saved bytes have the same "version" and loads them. Otherwise it will load the default values.
  if (EEPROM.read(CONFIG_START + 0) == CONFIG_VERSION[0] &&
      EEPROM.read(CONFIG_START + 1) == CONFIG_VERSION[1] &&
      EEPROM.read(CONFIG_START + 2) == CONFIG_VERSION[2])
    for (unsigned int t = 0; t < sizeof(storage); t++)
      *((char*)&storage + t) = EEPROM.read(CONFIG_START + t);
}

void saveConfig()
{
  for (unsigned int t = 0; t < sizeof(storage); t++)
    EEPROM.update(CONFIG_START + t, *((char*)&storage + t));
}


void setup()
{
  Serial.begin(115200);
  if (DBG)
  {
//    while (!Serial) {}    /*   WAIT FOR THE SERIAL CONNECTION FOR DEBUGGING   */
  }

  // DEBUG("Fans...");

  pinMode(SPD_IN, INPUT);
  pinMode(RELAY, OUTPUT);

  attachInterrupt(digitalPinToInterrupt(SPD_IN), pickRPM, FALLING);

  pwm6configure();

  loadConfig();
  storage.target = TARGET_SET;

  // DEBUG("PID...");
  // Setup the PID to work with our settings
  fanPID.SetSampleTime(WAIT);
//  fanPID.SetOutputLimits(DUTY_MIN - DUTY_DEAD_ZONE, 255);
  fanPID.SetOutputLimits(DUTY_MIN - DUTY_DEAD_ZONE, DUTY_MAX);

  fanPID.SetMode(AUTOMATIC);
// Relay (Bang-Bang) measured a=0.9deg, b=128, T_u=1000
// using Relay (Åström–Hägglund) method, K_u=4*b/pi/a = 181
  double aaa = 0.9;
  double bbb = 128;
  double T_u = 1000;
  double K_u = 4*bbb/3.1415/aaa;
// feeding this into Ziegler–Nichols method, with some arbitrary k-factors...
  double kp = 0.6*K_u/55; // 1.98
  double ki = 1.2*K_u/T_u; // 0.22
  double kd = 3*K_u*T_u/40/555; // 24.47
  fanPID.SetTunings(kp ,ki ,kd);
  // DEBUG(kp);
  // DEBUG("/");
  // DEBUG(ki);
  // DEBUG("/");
  // DEBUG(kd);

  // DEBUG("...Fans...");
  pwmSet6(DUTY_OD);
  // Let the fan run for 5s. Here we could add a fan health control to see if the fan revs to a certain value.
  delay(5000);

  // DEBUG("Sensor...");
  sensor.setup(TEMP_IN);

  // DEBUG("Ready.\n\n");
//  lc.clearDisplay(0);

  prev1 = millis();
}

void loop()
{
  unsigned long cur = millis();

// get temp from DHT22 sensor
  if (cur - prev3 >= sensor.getMinimumSamplingPeriod())
  {
    if (sensor.getStatus() == 0)
    {
      prev3 = cur;
      double t = sensor.getTemperature();
      /* Sometimes I get a checksum error from my DHT-22.
         To avoid exceptions I check if the reported temp is a number.
         This should work only with the "getStatus() == 0" above, but it gave me errors anyway, So I doublecheck */
      if (!isnan(t))
      {
        dtemp = round(t * 2.0) / 2.0;
        rtemp = t;
      }
    }
    else
    {
      // If there's an error in the sensor, wait 5 seconds to let the communication reset
      prev3 += 5000;
      sensor.setup(TEMP_IN);
      rtemp = 0;
    }
  }
// readout dvm and thermistor
// take average of N samples in a row, with a slight delay
  uint8_t i;
  float th_average = 0;
  float dvm_average = 0;
  for (i=0; i< NUMSAMPLES; i++) {
    th_average += analogRead(THERMISTORPIN);
    dvm_average += analogRead(DVMPIN);
    delay(10);
  }
  th_average /= NUMSAMPLES;
  dvm_average /= NUMSAMPLES;

  double tempK = log(SERIESRESISTOR * ((1024.0 / th_average - 1))); 
  th_average = 1.0 / (0.001129148 + (0.000234125 + (0.0000000876741 * tempK * tempK ))* tempK ) - 273.15;

  dvm_average = 4.744565217*0.010752688*dvm_average;
// we're now using the Thermistor reading
#ifdef THERMISTOR
  ctemp = round(th_average*10.0)/10.0;
#else
  ctemp = round(t*10.0)/10.0;
#endif

  fanPID.Compute(); // Do magic

  if (cur - prev1 >= WAIT)
  {
    prev1 = cur;
    unsigned long _duration = duration;
    unsigned long _ticks = ticks;
    duration = 0;

    // Calculate fan speed
    float Freq = (1e6 / float(_duration) * _ticks) / 2;
    speed = Freq * 60;
    ticks = 0;

    // Turn the fans ON/OFF
    duty_set = duty;
    if (ctemp < TEMP_MIN0) {
        fanRunning = false;
        duty_set = 0;
    }
    else if (round(duty) < DUTY_MIN) {
    // switch off fan and set duty to 0, or to DUTY_MIN if above min temp
      duty_set = 0;
      if (ctemp > TEMP_MIN) {
        fanRunning = true;
        duty_set = DUTY_MIN; 
      }
    }
    else {
    // > TEMP_MIN0 and > DUTY_MIN, make sure fan is switched on
      fanRunning = true;
    }
/* #define BANG-BANG */
#ifdef BANG-BANG
    if (ctemp > TARGET_SET ) { duty_set=DUTY_MAX; } else { duty_set = DUTY_MIN; }
    fanRunning = true;
#endif

// if above critical, or if we repeatedly could not read DHT temp, set to overdrive
    if (rtemp < 0.1 ) {
      if (notemp_cnt++ > 5) {
        duty_set = DUTY_OD;
        fanRunning = true;
      }
    } else {
      notemp_cnt = 0; 
    }
    if (ctemp > TEMP_CRITICAL) {
      duty_set = DUTY_OD;
      fanRunning = true;
    }
// now set fan and relay
    if (fanRunning) {  digitalWrite(RELAY, LOW); } else { digitalWrite(RELAY, HIGH);}
    PWM6 = duty_set; // set duty cycle

    if (debug10++ == DEBUG_FREQ)
    {
      if (debug10 > DEBUG_FREQ) debug10=1;
#define ANALYZE
#ifdef ANALYZE
      DEBUG(rtemp);
      DEBUG(", ");
      DEBUG(duty);
      DEBUG(", ");
      DEBUG(duty_set);
      DEBUG(", ");
      if (fanRunning) {
        DEBUG(speed+1);
      }
      else 
      {
        DEBUG(0.0);
      }
      DEBUG(", ");
      DEBUG(th_average);
      DEBUG(", ");
      DEBUG(dvm_average);
      DEBUG(", ");
      DEBUG(sensor.getStatusString());
#endif
#ifndef ANALYZE
      DEBUG(sensor.getStatusString());
      DEBUG(" - Target: ");
      DEBUG(storage.target);
      DEBUG(" - Temp: ");
      DEBUG(rtemp);
      DEBUG(" - Duty: ");
      DEBUG(map(round(duty), 0, DUTY_OD, 0, 100));
      DEBUG("% - Duty set: ");
      DEBUG (duty_set);
      DEBUG("/255 - Fan: ");
      if (fanRunning) {
        DEBUG("running speed ");
        DEBUG(speed);
      }
      else 
      {
        DEBUG("stopped");
      }
#endif
      DEBUG("\n");
    }
  }

}
