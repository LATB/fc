#!/usr/bin/env python

import sys
import pandas as pd
import numpy as np
import datetime
from scipy.stats import linregress
from math import log2, ceil, sqrt
import matplotlib.pyplot as plt
from datetime import timedelta, date
from imgcat import imgcat

ymax = 83
# ymax = 48

# Import the data
# cvs created from https://covidtracking.com/api/, https://covidtracking.com/api/us/daily.csv
# df = pd.read_csv('neu.csv', header=0, names=['Temp','Duty','Set','Fan','Status'])
df = pd.read_csv(sys.stdin, header=0, names=['Temp','Duty','Set','Fan','Th','DVM','Status'])

# Extend the table with some "normalized" temperature
nt = 40.0 # plot NTemp as 20..40 deg in plot b/w 39 and 40 deg
# df['NTemp'] = 4.0*(df.Temp-nt)/nt+nt - 38.5
# df['NTh'] = 4.0*(df.Th-nt)/nt+nt - 38.5
len = len(df)
sTemp = df.Temp[len-1]
df['NTemp'] = (df.Temp - sTemp)*5 + sTemp
sTh = df.Th[len-1]
df['NTh'] = (df.Th - sTh)*5 + sTh
df['NDuty'] = df.Duty/12.0 - 3.0
df['NSet'] = df.Set/12.0 - 3.0
df['NSet2'] = df.Set/10.-10.0+38.0 - 39.5
# df['Temp'] = df.Temp - 38.5
# df['Th'] = df.Th - 44.5
df['Bias'] = df.DVM/0.974 # in V
# Plot the data

fig, ax = plt.subplots()

dt = 5.0/3600.0
t = np.arange(-len*dt, 0.0, dt)

ax.plot(t, df['NTemp'],color='xkcd:green', linewidth=0.3)
ax.plot(t, df['NTh'],color='xkcd:darkgreen', linewidth=0.3)
ax.plot(t, df['Temp'],color='xkcd:azure', linewidth=1.5)
ax.plot(t, df['Th'],color='xkcd:blue', linewidth=1.5)
ax.plot(t, df['NSet'],color='xkcd:orange', linewidth=1.5)
ax.plot(t, df['NDuty'],color='xkcd:grey', linewidth=0.5)
#### ax.plot(df['NSet2'],color='xkcd:orange', linewidth=0.5)
# ax.plot(t, df['Bias'],color='xkcd:orange', linewidth=0.5)

ax.axis([-len*dt, 50*dt, 0, ymax])
ax.set(xlabel='time passed [hours]', ylabel='Temperature & Fan Control',
       title='Temperature on Heat Sink')

# ax.axis([-50*dt,len*dt,1.9,7.9])
# ax.set(xlabel='time passed [hours]', ylabel='V_bias [V]',
#        title='Bias')

ax.grid()

dt = datetime.datetime.now().strftime("%X on %B %d, %Y") 
ax.text(0.8, 0.95, dt, ha='center', transform=ax.transAxes)

fig.savefig('fc.pdf', format='pdf')

imgcat(fig, height=30)
