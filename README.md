# fc

Ardunino-based fan control for audio amp.

This is `fc`, a fan controller using PID logic and PWM signals to control PC fans,  based on  the github project [fancontrol](https://gitlab.com/bonnee/fancontrol). `fancontrol` is copyright 2017 Matteo Bonora (bonora.matteo@gmail.com). Detailed information is available there.

Modifications by LATB, provided as is:
- Optimized PID parameters using the [Ziegler–Nichols method](https://en.wikipedia.org/wiki/Ziegler–Nichols_method), based on a bang-bang controller measurement of my setup, with some tuning -- I probably should repeat that some time as I still see some controller oscillations
- The temperature measurement is based on a thermistor mounted on the heat sink (although the DHT22 is also connected)
- Log output to serial so that temperature curves can be plotted using the `fc.py` script
- Other modifications, like overheating protection and other things

The setup is based around SparkFun pro micro with periphery as shown in the schematic, connected via USB to a Raspberry Pi running a python monitoring scrip.

`arduino-cli` is installed on the RPi, compile the fc/fc.ino firmware like this:

    arduino-cli compile --fqbn=SparkFun:avr:promicro:cpu=16MHzatmega32U4

then upload firmware to the pro micro

    arduino-cli upload -p /dev/ttyACM0 --fqbn=SparkFun:avr:promicro:cpu=16MHzatmega32U4

If USB is connected the pro micro writes a temp log (csv) to the USB serial port, and the
python scrip `fc.py` can be used, e.g. on the RPi, to plot the temp curves. It
produces a .pdf file that then e.g. can be copied to your laptop or Dropbox
etc.

Here is how I currently running this (I installed `zsh` on the RPi for convenience):
get your ssh token so that scp to laptop etc works, then background reading from
USB into csv file `fc.dat`

    eval `ssh-agent` && ssh-add && cat < /dev/ttyACM0 > fc.dat &

Then run a loop every n seconds (e.g. n=60) to execute the python script producing
the pdf, which then gets copied to a laptop/Dropbox, from where you can monitor
it -- also, if you are using iterm you will see the resulting graph in your
iterm window (otherwise I suppose you'll have to comment out a line in `fc.py`)

    while sleep 60; do date; tac fc.dat | head -4000 | python3 fc.py; scp fc.pdf <name@your.host>:Dropbox/; done

That's it!

You can find me on diyaudio.com as @LATB



